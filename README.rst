Satellite LOS NZ
******************
A Python 3.5 Jupyter notebook for computing satellite lines-of-sight across New Zealand for various geostationary satellites.
Also includes the output of the computations (GeoTIFF files) in the ``output`` directory.
Uses the `Wavetrace library <https://github.com/NZRS/wavetrace>`_ and GDAL; see ``requirements.txt`` for more details on dependencies.
